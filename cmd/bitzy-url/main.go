package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/narbeh/bitzy-url/cmd/bitzy-url/app"
	"gitlab.com/narbeh/bitzy-url/internal/factories"
)

// @title Bitzy URL API
// @version 1.0
// @description Bitzy URL shortener website

// @host localhost:7777

func main() {
	fmt.Printf("[Bitzy URL] Running...\n")

	// Connection pool
	pool := factories.NewConnectionPool([]string{os.Getenv("SHARD_0"), os.Getenv("SHARD_1")})

	http.ListenAndServe(os.Getenv("PORT"), app.InitRoutes()) //:7777

	//Don't forget to clean up!
	for _, conn := range pool.Conn {
		defer conn.Close()
	}
}
