package app

import (
	"context"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"gitlab.com/narbeh/bitzy-url/internal/models"
	"gitlab.com/narbeh/bitzy-url/internal/repos"
)

func InitRoutes() http.Handler {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("PONG"))
	})

	r.Route("/url", func(r chi.Router) {
		// @description POST /url
		// @description Store original URL and generate Shortener Hash for it
		// @Success 200 models.URL
		r.Post("/", CreateUrl)

		// @description GET /url/{hash}
		// @description Retrieve full URL object based on Hash
		// @Success 200 models.URL
		r.Route("/{hash}", func(r chi.Router) {
			r.Use(UrlCtx)
			r.Get("/", GetUrl)
		})
	})

	return r
}

func UrlCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		var urlResponse *models.URLResponse

		if hash := chi.URLParam(r, "hash"); hash != "" {
			urlResponse, err = repos.GetUrl(hash)
		}

		if err != nil {
			render.Render(w, r, ErrNotFound)
			return
		}

		ctx := context.WithValue(r.Context(), "URLResponse", urlResponse)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func GetUrl(w http.ResponseWriter, r *http.Request) {
	urlResponse := r.Context().Value("URLResponse").(*models.URLResponse)

	if err := render.Render(w, r, urlResponse); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func CreateUrl(w http.ResponseWriter, r *http.Request) {
	data := &models.URL{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	urlResponse, err := repos.AddUrl(data)
	if err != nil {
		render.Render(w, r, ErrConflict)
		return
	}

	render.Status(r, http.StatusCreated)
	if err := render.Render(w, r, urlResponse); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

func ErrRender(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 422,
		StatusText:     "Error rendering response.",
		ErrorText:      err.Error(),
	}
}

var ErrNotFound = &ErrResponse{HTTPStatusCode: 404, StatusText: "Resource not found."}
var ErrConflict = &ErrResponse{HTTPStatusCode: 409, StatusText: "Resource processing failed."}
