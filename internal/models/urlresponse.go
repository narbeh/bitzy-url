package models

import (
	"encoding/json"
	l "log"
	"net/http"
)

type ResponseSrc string

const (
	ResponseSrcDatabase ResponseSrc = "Database"
	ResponseSrcCache                = "Cache"
)

type URLResponse struct {
	Data   []*URL      `json:"data,omitempty"`
	Shard  string      `json:"shard,omitempty"`
	Source ResponseSrc `json:"source,omitempty"`
}

//Implements Model interface

func (m *URLResponse) Marshal() ([]byte, error) {
	//object to bytes
	b, err := json.Marshal(m)
	if err != nil {
		l.Printf("URLResponse Marshal err: %v", err)
		return nil, err
	}

	return b, nil
}

func (m *URLResponse) Unmarshal(data []byte) error {
	//json bytes to url object
	err := json.Unmarshal(data, &m)
	if err != nil {
		l.Printf("URLResponse Unmarshal err: %v", err)
		return err
	}

	return nil
}

func (m *URLResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (u *URLResponse) Bind(r *http.Request) error {
	return nil
}
