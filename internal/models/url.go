package models

import (
	"encoding/json"
	l "log"
	"net/http"
	"time"
)

type URL struct {
	ID          int       `db:"id" json:"-"`
	Hash        string    `json:"hash"`
	OriginalUrl string    `json:"original_url"`
	UserId      int       `json:"user_id"`
	ExpiresAt   time.Time `json:"expires_at,omitempty"`
	CreatedAt   time.Time `json:"-"`
}

//Implements Model interface

func (m *URL) Marshal() ([]byte, error) {
	//object to bytes
	b, err := json.Marshal(m)
	if err != nil {
		l.Printf("URL Marshal err: %v", err)
		return nil, err
	}

	return b, nil
}

func (m *URL) Unmarshal(data []byte) error {
	//json bytes to url object
	err := json.Unmarshal(data, &m)
	if err != nil {
		l.Printf("URL Unmarshal err: %v", err)
		return err
	}

	return nil
}

func (m *URL) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (u *URL) Bind(r *http.Request) error {
	return nil
}
