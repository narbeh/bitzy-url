package interfaces

import "net/http"

type Model interface {
	Marshal() ([]byte, error)
	Unmarshal(data []byte) error
	Render(w http.ResponseWriter, r *http.Request) error
	Bind(r *http.Request)
}
