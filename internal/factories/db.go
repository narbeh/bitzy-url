package factories

import (
	"context"
	l "log"
	"sync"

	"github.com/jackc/pgx/v4/pgxpool"
)

var _dOnce sync.Once
var _dInstance *ConnectionPool

type ConnectionPool struct {
	Conn []*pgxpool.Pool
}

func NewConnectionPool(shardMap []string) *ConnectionPool {
	_dOnce.Do(func() {
		//create a connection pool based on shard map
		var connMap []*pgxpool.Pool
		for _, dsn := range shardMap {
			conn, err := pgxpool.Connect(context.Background(), dsn)
			if err != nil {
				l.Fatalf("Unable to connection to database: %v\n", err)
			}

			connMap = append(connMap, conn)
		}

		_dInstance = &ConnectionPool{connMap}
	})

	return _dInstance
}

func GetConnectionPool() *ConnectionPool {
	return _dInstance
}
