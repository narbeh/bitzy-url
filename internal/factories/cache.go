package factories

import (
	"context"
	l "log"
	"sync"

	"github.com/go-redis/redis/v8"
)

var _cOnce sync.Once
var _cInstance *Cache

type Cache struct {
	redisCln *redis.Client
}

func GetCacheInstance() *Cache {
	_cOnce.Do(func() {
		rdb := redis.NewClient(&redis.Options{
			Addr:     "redis:6379",
			Password: "",
			DB:       0,
		})
		_cInstance = &Cache{rdb}
	})

	return _cInstance
}

func (c *Cache) GetValueFromCache(key string) (string, error) {
	val, err := c.redisCln.Get(context.Background(), key).Result()
	if err != nil {
		l.Printf("GetValueFromCache err: %+v", err)
		return "", err
	}

	return val, nil
}

func (c *Cache) SetValueToCache(key string, value string) error {
	err := c.redisCln.Set(context.Background(), key, value, 0).Err()
	if err != nil {
		l.Printf("SetValueToCache err: %+v", err)
		return err
	}

	return nil
}
