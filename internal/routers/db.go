package routers

import (
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/narbeh/bitzy-url/internal/factories"
)

type ShardConnection struct {
	Connection *pgxpool.Pool
	Shard      string
}

func GetConnectionForHash(hash string, pool *factories.ConnectionPool) (*ShardConnection, error) {
	runes := []rune(hash)
	charNum := int(runes[0])
	shardId := charNum % len(pool.Conn)

	return &ShardConnection{Connection: pool.Conn[shardId], Shard: fmt.Sprintf("SHARD_%d", shardId)}, nil
}
