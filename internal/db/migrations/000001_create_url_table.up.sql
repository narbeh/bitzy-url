create table url
(
	id serial,
	hash varchar(8) not null,
	original_url varchar(512) not null,
	user_id int not null,
	expires_at timestamp default now(),
	created_at timestamp default now()
);

create unique index URL_hash_uindex
	on url (hash);

alter table url
	add constraint URL_pk
		primary key (id);


