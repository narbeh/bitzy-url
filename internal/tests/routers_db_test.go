package tests

import (
	"os"
	"testing"

	"gitlab.com/narbeh/bitzy-url/internal/factories"
	"gitlab.com/narbeh/bitzy-url/internal/routers"
)

func TestGetConnectionForHash(t *testing.T) {
	hash := "af2dccfd"
	pool := factories.NewConnectionPool([]string{os.Getenv("SHARD_0"), os.Getenv("SHARD_1")})

	shardConn, _ := routers.GetConnectionForHash(hash, pool)

	if shardConn.Shard != "SHARD_1" {
		t.Fatalf("GetConnectionForHash: Failed. shardConn: %+v", shardConn)
	}

	t.Logf("GetConnectionForHash: Pass. shardConn: %+v", shardConn)
}
