package repos

import (
	"context"
	"errors"
	l "log"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"gitlab.com/narbeh/bitzy-url/internal/factories"
	"gitlab.com/narbeh/bitzy-url/internal/models"
	"gitlab.com/narbeh/bitzy-url/internal/routers"
)

func GetUrl(hash string) (*models.URLResponse, error) {
	//try to fetch from cache first
	v, err := factories.GetCacheInstance().GetValueFromCache(hash)
	if len(v) > 0 {
		urlResponse := models.URLResponse{}
		err = urlResponse.Unmarshal([]byte(v))
		if err != nil {
			l.Printf("GetUrl > urlResponse.Unmarshal([]byte(v)) err: %v, v: %+v", err, v)
		} else {
			urlResponse.Source = models.ResponseSrcCache
			l.Printf("GetUrl > urlResponse.Unmarshal([]byte(v)) read from cache: %+v", urlResponse)
			return &urlResponse, nil
		}
	}

	//get connection to shard holiding the url according to the hash
	conn, err := routers.GetConnectionForHash(hash, factories.GetConnectionPool())
	if err != nil {
		l.Printf("GetUrl > GetConnectionForHash err: %v", err)
		return nil, err
	}

	//retrieve url row from database
	var urls []*models.URL
	err = pgxscan.Select(context.Background(), conn.Connection, &urls, `SELECT * FROM url WHERE hash = $1 LIMIT 1`, hash)
	if err != nil {
		l.Printf("GetUrl > pgxscan.Select err: %v", err)
		return nil, err
	}

	if len(urls) == 0 {
		return nil, errors.New("GetUrl > Resource not found.")
	}

	urlResponse := &models.URLResponse{Data: urls, Shard: conn.Shard}

	//cache it
	b, err := urlResponse.Marshal()
	if err != nil {
		l.Printf("GetUrl > urlResponse.Marshal() err: %v", err)
	} else {
		value := string(b)
		factories.GetCacheInstance().SetValueToCache(hash, value)
		l.Printf("GetUrl > SetValueToCache() hash: %v, value: %+v", hash, value)
	}

	urlResponse.Source = models.ResponseSrcDatabase
	return urlResponse, nil
}

func AddUrl(m *models.URL) (*models.URLResponse, error) {
	hash := GetHashForOriginalUrl(m.OriginalUrl, m.UserId)

	if len(hash) == 0 {
		return nil, errors.New("AddUrl > Unable to generate hash")
	}

	m.Hash = hash
	m.ExpiresAt = time.Now().Add(24 * time.Hour)

	conn, err := routers.GetConnectionForHash(m.Hash, factories.GetConnectionPool())
	if err != nil {
		l.Printf("AddUrl > GetConnectionForHash err: %v", err)
		return nil, err
	}

	var urlID int
	row := conn.Connection.QueryRow(context.Background(), `INSERT INTO url (hash, original_url, user_id, expires_at) VALUES ($1, $2, $3, $4) RETURNING id`, m.Hash, m.OriginalUrl, m.UserId, m.ExpiresAt)
	err = row.Scan(&urlID)
	if err != nil {
		l.Printf("AddUrl > conn.Connection.QueryRow err: %v", err)
		return nil, err
	}

	return &models.URLResponse{Data: []*models.URL{m}, Shard: conn.Shard}, nil
}
