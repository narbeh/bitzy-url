package repos

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"time"
)

func GetHashForOriginalUrl(originalUrl string, userId int) string {
	//generate md5 hash by concat url and user id
	t := time.Now()
	hash := md5.Sum([]byte(fmt.Sprintf("%s|%d|%d-%02d-%02dT%02d:%02d:%02d", originalUrl, userId, t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())))
	md5Str := hex.EncodeToString(hash[:])

	//get a substring of length 8
	runes := []rune(md5Str)
	return string(runes[0:8])
}
