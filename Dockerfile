FROM golang:1.15

WORKDIR $GOPATH/src/gitlab.com/narbeh/bitzy-url
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["bitzy-url"]
